import { StreetAppPage } from './app.po';

describe('street-app App', () => {
  let page: StreetAppPage;

  beforeEach(() => {
    page = new StreetAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
