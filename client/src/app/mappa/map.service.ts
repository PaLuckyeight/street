import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class MapService {

  murales = this._http.get(`
  graphql?query=%7B%0A%20%20allMurales%20%7B%0A%20%20%20%20id%0A%20%20%7D%0A%7D
`).subscribe( (res: Response) => {console.table(res.json()); return res.json()})

  constructor(private _http: Http) { }

  getMurales() {
    return this.murales
  }
}
