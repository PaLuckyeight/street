import { Injectable } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import gql from 'graphql-tag';


enum FromPage {
  TAT=0,
  STREET=1
}

@Injectable()
export class GeoService {


  queryStudios=gql`
         query A($coords:[Float]!){
          geoStudios(coords: $coords) {
            name
            loc{
              coordinates
              }
            
          }
        }`;

  constructor(private apollo: Apollo, ) { }

  findNear(From:FromPage ,coords: [number]){

    if (From == FromPage.TAT) {
      return this.apollo.query({

        query: this.queryStudios,
        variables: {
          coords: coords
        }
      });
    }
    else{
      console.log('STREEEEEEET ')
      // chiamata alla find da componenti street-art
    }
  }



}
