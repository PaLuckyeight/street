import { Injectable } from '@angular/core';
import {LatLngBounds} from "leaflet";
import {Http, Headers, Response} from "@angular/http";

import "rxjs/add/operator/mergeMap";

export interface ILatLng {
  latitude: number;
  longitude: number;
}

export class Location implements ILatLng {
  latitude: number;
  longitude: number;
  address: string;
  viewBounds: LatLngBounds;
}

@Injectable()
export class GeocodingService {

  http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  geocode(address: string) {
    return this.http
      .get("http://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(address))
      .map(res => res.json())
      .map(result => {
        if (result.status !== "OK") { throw new Error("unable to geocode address"); }

        let location = new Location();
        location.address = result.results[0].formatted_address;
        location.latitude = result.results[0].geometry.location.lat;
        location.longitude = result.results[0].geometry.location.lng;

        let viewPort = result.results[0].geometry.viewport;
        location.viewBounds = L.latLngBounds(
          {
            lat: viewPort.southwest.lat,
            lng: viewPort.southwest.lng},
          {
            lat: viewPort.northeast.lat,
            lng: viewPort.northeast.lng
          });

        return location;
      });
  }

  /* getCurrentLocationOne(){
    navigator.geolocation.getCurrentPosition(success, error);

    function success(pos) {
      var crd = pos.coords;
      console.log('Your current position is:');
      console.log(`Latitude : ${crd.latitude}`);
      console.log(`Longitude: ${crd.longitude}`);
      console.log(`More or less ${crd.accuracy} meters.`);
      //appMap2.panTo([crd.latitude, crd.longitude]);
      let location = new Location();

      location.latitude = crd.latitude;
      location.longitude = crd.longitude;

      return location;
    };

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    };


  }*/

  getCurrentLocation() {
    return this.http
      .get("http://ipv4.myexternalip.com/json")
      .map(res => res.json().ip)
      .mergeMap(ip => this.http.get("http://freegeoip.net/json/" + ip))
      .map((res: Response) => res.json())
      .map(result => {
        let location = new Location();

        location.address = result.city + ", " + result.region_code + " " + result.zip_code + ", " + result.country_code;
        location.latitude = result.latitude;
        location.longitude = result.longitude;

        return location;
      });
  }

}
