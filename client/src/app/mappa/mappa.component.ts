declare var require: any
import { Component, OnInit } from '@angular/core';
import { MapService } from './map.service';
import { GeocodingService } from './geocoding.service';
import * as L from 'leaflet';
import 'leaflet-providers';

export interface ILatLng {
  latitude: number;
  longitude: number;
}

export class Location implements ILatLng {
  latitude: number;
  longitude: number;
  address: string;
  viewBounds: L.LatLngBounds;
}

@Component({
  selector: 'st-mappa',
  templateUrl: './mappa.component.html',
  styleUrls: ['./mappa.component.styl'],
  providers: [MapService,GeocodingService]
})
export class MappaComponent implements OnInit {

  title = 'app';
  murales: any;
  appMap: any;
  image: any;
  icon: any;
  location={};
  options: any;
  constructor(private m: MapService , private geocoder: GeocodingService) {
    //this.image = require('./Logo-graff2.jpg');
    this.image = require('./street-art.svg');
  }

  ngOnInit() {
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(position => {
        this.location = position.coords;
        console.log(position.coords);
        this.appMap.panTo([position.coords.latitude, position.coords.longitude]);
      });
    }

    this.murales = this.m.getMurales();

    this.appMap = L.map('map', {
      zoomControl: false,
      //center: L.latLng(44.4938100, 11.3387500),
      center: L.latLng(41.9028, 12.4964),
      zoom: 13,
      minZoom: 4,
      scrollWheelZoom: 'center',
      layers: [ ],
    });

    this.icon = L.icon({
      iconUrl: this.image,
      iconSize:     [25, 41], // size of the icon
      iconAnchor:   [25, 41], // point of the icon which will correspond to marker's location
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    L.marker([44.49381, 11.3387500], {icon: this.icon}).addTo(this.appMap);
    L.marker([44.528369, 11.366745], {icon: this.icon}).addTo(this.appMap);
    L.marker([44.51388, 11.3587500], {icon: this.icon}).addTo(this.appMap);
    L.tileLayer.provider('CartoDB.DarkMatter').addTo(this.appMap);




  }





}
