import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'app';
  options = {
	  layers: [
		  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
	  ],
	  zoom: 5,
	  center: L.latLng({ lat: 38.991709, lng: -76.886109 })
  };
}
