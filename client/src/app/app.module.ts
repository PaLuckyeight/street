import { LeafletModule } from '@asymmetrik/angular2-leaflet';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MdAutocompleteModule, MdInputModule, MdCardModule, MdButtonModule } from '@angular/material';
import { AppComponent } from './app.component';
import { MappaComponent } from './mappa/mappa.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApolloModule } from 'apollo-angular';
import { provideClient } from './apolloclient';
import { TattooComponent } from './tattoo/tattoo.component';
import { TattoomapComponent } from './tattoomap/tattoomap.component';


@NgModule({
  declarations: [
    AppComponent,
    MappaComponent,
    HomeComponent,
    TattooComponent,
    TattoomapComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdButtonModule,
    MdAutocompleteModule,
    MdInputModule,
    LeafletModule,
    ApolloModule.forRoot(provideClient),
    RouterModule.forRoot([
      { path: '', component: HomeComponent},
      { path: 'mappa', component: TattoomapComponent},
      { path: 'tat', component: TattooComponent},
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
