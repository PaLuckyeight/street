import { Injectable } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable()
export class TattooService {

  dati: any;
  loading = true;
  unique : string;
  mutation = gql`
      mutation Addtatuatore($name: String , $surname: String, $nomestudio: String , $uniquename: String! , $siteurl: String) {
        AddTatuatore(name: $name, surname: $surname, nomestudio: $nomestudio, uniquename:$uniquename , siteurl: $siteurl)
   }`;

  constructor(private apollo: Apollo) {
  }

  addTatuatore(name: string, surname:string ,nomestudio: string, uniquename:string, siteurl:string) {
    this.unique= (uniquename ? uniquename : (name + ' ' + surname) );
    return this.apollo.mutate({
      mutation: this.mutation,
      variables: {
        name: name,
        surname: surname,
        uniquename: this.unique,
        nomestudio:nomestudio,
        siteurl: siteurl
      }
    });
  }


  allStudios(){
    return this.apollo.query({

        query: gql`
           query { allStudios { name } }
        `
    });
  }

}
