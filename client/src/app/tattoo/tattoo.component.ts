import { Component, OnInit } from '@angular/core';
import {  Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { TattooService } from "./tattoo.service";
import { ValidatorsService } from "./validators.service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';

import {Observable} from 'rxjs/Observable';

import { IArtistOutput,ITattooStudio } from '../../../../server/src/schemas/Tattoo';

@Component({
  selector: 'st-tattoo',
  templateUrl: './tattoo.component.html',
  styleUrls: ['./tattoo.component.styl'],
  providers: [TattooService, ValidatorsService]
})
export class TattooComponent implements OnInit {
  dati: any;
  loading = true;
  FilteredStudios: any;
  Studios: Array<any>;
  tatuatoreForm : FormGroup;
  myControl = new FormControl();
  array: Array<any> =[];


  constructor(private TatServ: TattooService , private fb: FormBuilder, private ValServ: ValidatorsService) {

  }

  ngOnInit() {

    this.buildForm();



     this.TatServ.allStudios().subscribe(({data, loading}:  any)  => {
        this.Studios = data.allStudios;
        this.loading = loading;
        this.Studios.forEach(  (el) => {
          this.array.push(el.name);
        })
        console.log(` ${this.Studios} + ${this.array} ` );
       this.FilteredStudios = this.tatuatoreForm.get('nomestudio').valueChanges
         .startWith(null)
         .map(val => val ? this.filter(val) : this.array.slice());


      });



  }
  buildForm(): void {
    this.tatuatoreForm = this.fb.group ({
      name: '',
      surname: '',
      nomestudio:'',
      uniquename:'',
      siteurl:''
    }, {validator: this.ValServ.correctInput('name', 'surname','uniquename')});


  }


  filter(val: string): string[] {
    return this.array.filter(option => new RegExp(`^${val}`, 'gi').test(option));
  }

  onSubmit({ value, valid }: { value: IArtistOutput, valid: boolean }) {
    console.log(value, valid);


    if(valid) {
      this.TatServ.addTatuatore(value.name, value.surname, value.nomestudio, value.uniquename, value.siteurl).subscribe(({data, loading}: any) => {
        this.dati = data;
        this.loading = loading;
        console.log(this.dati);
      });
    }
    else{
      console.log("ERROR: form invalido");
    }

  }

}
