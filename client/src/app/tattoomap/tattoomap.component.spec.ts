import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TattoomapComponent } from './tattoomap.component';

describe('TattoomapComponent', () => {
  let component: TattoomapComponent;
  let fixture: ComponentFixture<TattoomapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TattoomapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TattoomapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
