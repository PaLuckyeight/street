import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { TattooService } from "../tattoo/tattoo.service";
import { GeoService } from "../mappa/geo.service";
//import * as path from "path";

import * as lp from 'leaflet';

import { MappaComponent} from "../mappa/mappa.component"
import { IStudioOutput ,IArtistOutput} from '../../../../server/src/schemas/Tattoo';



@Component({
  selector: 'st-tattoomap',
  templateUrl: './tattoomap.component.html',
  styleUrls: ['./tattoomap.component.styl'],
  providers: [GeoService]
})
export class TattoomapComponent implements AfterViewInit {

  coords: [number,number] = [0,0];
  dati: any;
  loading = true;
  array: Array<IStudioOutput> =[];
  buttonMsg='';
  @Input() myPopu=`<!DOCTYPE html>
    <html>
    <head>
      <style>
      h1 {
      color: red;
    }
    </style>
    </head>

    <body>
      <h1>Popup title</h1>
    <input type="text">
      <p>Popup content</p>
    </body>
    </html>`;
  //mypopup=path.join(__dirname, "myPopup.html");


  constructor(private geoServ: GeoService) { }

  @ViewChild(MappaComponent)
  private mapref: MappaComponent;

  ngAfterViewInit() {
    let TheBounds = this.mapref.appMap.getCenter();
    this.coords[0]=TheBounds.lat;
    this.coords[1]=TheBounds.lng;

    this.findNearestStudios();

  }


  findNearestStudios(){
     return this.geoServ.findNear(0,this.coords).subscribe(({data, loading}:  any)  => {
      this.dati = data.geoStudios;
      this.loading = loading;
      this.dati.forEach(  (el) => {
        this.array.push(el);
        lp.marker(el.loc.coordinates, {icon: this.mapref.icon}).addTo(this.mapref.appMap)
          .bindPopup(el.name);
      })
      console.log(` ${this.dati} + ${JSON.stringify(this.array)} ` );
      //lp.marker([44.503390,  11.745063], {icon: this.mapref.icon}).addTo(this.mapref.appMap);

    });

  }


  addStudio(){
      //L.DomUtil.addClass(this.mapref.appMap._container,'crosshair-cursor-enabled');
      this.buttonMsg= 'Clicca sulla mappa per le coordinate dello studio';
      this.mapref.appMap.once('click', this.onMapClick.bind(this));

      // TODO : "Rifare in observable, no on"
  }


  onMapClick(e) {
    console.log("You clicked the map at " + e.latlng.toString());
    this.popUp(e.latlng);
    //lp.marker(e.latlng, {icon: this.mapref.icon}).addTo(this.mapref.appMap)

  }

  popUp ( latlng: any){
    L.popup()
      .setLatLng(latlng)
      .setContent('<iframe id="iframe" src="{{myPopu}}"  ></iframe>')
      .openOn(this.mapref.appMap);
  }

}
