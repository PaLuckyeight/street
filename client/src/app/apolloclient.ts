import { ApolloClient, createNetworkInterface } from 'apollo-client';

const client: ApolloClient = new ApolloClient();
export function provideClient(): ApolloClient {return client};
