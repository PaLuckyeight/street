import { GeoJsonObject } from "geojson";
import { IPerson } from "./Person";
declare const Buffer:any

const enum TattooStyle {
  OLDSCHOOL,
  REALISTIC
}

export interface ITattooArtist extends IPerson {
  style: TattooStyle[];
  siteurl: string;
}

export interface IImage {
  data:  any,
  contentType: "image/png"
}

export interface ITattooStudio {
  id: string;
  name?: string;
  address?: string;
  logo:IImage;
  studiophoto:IImage;
  tatuatori: ITattooArtist[];
  loc?: {type: string, coordinates: number[]};
}

export interface IArtistOutput {
  style: TattooStyle[];
  name?: string;
  surname?: string;
  nomestudio: string;
  uniquename: string;
  siteurl: string;
}
export interface IStudioOutput {
  name: string;
  tatuatori: IArtistOutput[] | null;
  loc: { coordinates: number[]};
}