import { GeoJsonObject } from "geojson";
import { IPerson } from "./Person";

enum ArtType {
  MURALES,
  STENCIL
}

export interface IArt {
  id: String;
  artists: IArtist[];
  type: ArtType;
  geometry: GeoJsonObject;
}

export interface IArtist extends IPerson {
  artName: String;
}