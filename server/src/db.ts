import * as mongoose from "mongoose";

const uri: string = process.env.MLAB_URI2  || "";

mongoose.connect(uri).catch((err: mongoose.Error) => {
  console.error(err);
  process.exit(-1);
});

const db: mongoose.Connection = mongoose.connection;

db.on("error", (err) => console.error(`connection error:${err}`));
db.once("open", (ref) => console.log("* MongoDB connected *"));

export const DB: mongoose.Connection = db;