import * as dotenv from "dotenv";
dotenv.config();

import * as bodyParser from "body-parser";
import * as express from "express";
import * as path from "path";
import { ApolloServer } from "./apollo";


export class Server {

  app: express.Express;
  graphqlServer: ApolloServer;

  constructor() {
    this.app = express();
    this.setupMiddleware();
    this.app.use(express.static(path.join(__dirname, "../public/")));
    this.app.get("*", (req, res) => {
      res.status(200).sendFile(path.join(__dirname, "../public/"));
    });
    this.graphqlServer = new ApolloServer(this.app, 4200);
  }

  private setupMiddleware(): void {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
  }
}

export const server: express.Express = new Server().app;