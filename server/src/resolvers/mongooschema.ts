import * as mongoose from "mongoose";



const Schema: typeof mongoose.Schema = mongoose.Schema;

function capitalize (val:any) {
  if (typeof val !== 'string') val = '';
  return val.charAt(0).toUpperCase() + val.substring(1);
}

export const ArtistaSchema: mongoose.Schema = new mongoose.Schema({
  _id: { type: Schema.Types.ObjectId, unique: true, required: true},
  artname: { type: String, unique: true, required: true}
});

export const MuralesSchema: mongoose.Schema = new mongoose.Schema ({
        _id : { type: Schema.Types.ObjectId, unique: true, required: true},
        artista: { type: [Schema.Types.ObjectId], ref: ArtistaSchema},
        type: { type: String, unique: false}
});

export const TatuatoreSchema: mongoose.Schema = new mongoose.Schema({
  _id: { type: Schema.Types.ObjectId, unique: true, required: true},
  name: { type: String, unique: false,required: false, maxlength:100, set: capitalize},
  surname: { type: String, unique: false,required: false, maxlength:100, set: capitalize},
  uniquename: { type: String, unique: true,required: true, set: capitalize},
  stile: { type: String, unique: false,required: false},
  siteurl: { type: String, unique: false,required: false}
});


export const ImageSchema: mongoose.Schema = new mongoose.Schema({
  img: { data: Buffer, contentType: String }
});


export const StudioTatSchema: mongoose.Schema = new mongoose.Schema ({
  _id : { $type: Schema.Types.ObjectId, unique: true, required: true},
  name: { $type: String , unique: true, required: true ,max:100, minlength:3, set: capitalize},
  address: { $type: String , unique: true, required: true},
  logo: {    $type: ImageSchema , unique: true  },
  studiophoto: {    $type: ImageSchema , unique: true  },
  tatuatori: { $type: [Schema.Types.ObjectId], ref: TatuatoreSchema},
  loc: { type: String, coordinates: [Number] }

}, { typeKey: "$type" });

StudioTatSchema
  .index({loc: "2dsphere"})
  .virtual('url')
    .get(function () {
      return '/studios/${this._id}';
    });

export const Artist: mongoose.Model<mongoose.Document> = mongoose.model("Artist", ArtistaSchema);
export const Murals: mongoose.Model<mongoose.Document> = mongoose.model("Murales", MuralesSchema);
export const Studios: mongoose.Model<mongoose.Document> = mongoose.model("StudioTat", StudioTatSchema);
export const Tatuatore: mongoose.Model<mongoose.Document> = mongoose.model("Tatuatore", TatuatoreSchema);
