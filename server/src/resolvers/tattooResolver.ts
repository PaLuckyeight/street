import { Studios, Tatuatore } from './mongooschema'
import { model, Connection , Types} from 'mongoose'


export const TattooResolvers = {
  Query : {
    allStudios() {
      return Studios.find(function (err, Studios) {
        if (err) return console.error(err);
        console.log(Studios);
      });
    },

    geoStudios: function(coords: [number], res : any){
      return Studios.find().where('loc').near({ center: {type : 'Point' , coordinates: [44.49381, 11.3387500]  }, maxDistance: 50000 }).limit(10).exec(function(err, locations) {
        if (err) {
          return console.error(err);
        }
        console.log(res);
      });
    }
  },
  Mutation : {
    async AddStudio (obj: void, { name , loc }: { name: string, loc: {coordinates: [number], type: string} }, context:  Connection){

      const newstudio = new Studios({
        _id: Types.ObjectId(),
        name: name,
        loc: loc,
      });
      await ( newstudio.save(function (err, createdTodoObject)
        {
          if (err) {
            console.log(err);
          }
          // This createdTodoObject is the same one we saved, but after Mongo
          // added its additional properties like _id.
          console.log(createdTodoObject);
        })
      );
    },

    async AddTatuatore (obj: void, { name ,surname, stile , nomestudio , uniquename, siteurl}: { name?: string, surname?: string , stile: [string] , nomestudio?: string, uniquename:string , siteurl?:string}, context: Connection) {

      const newtatuatore = new Tatuatore({
        _id: Types.ObjectId(),
        name: name,
        surname: surname,
        uniquename: uniquename,
        stile: stile,
        siteurl: siteurl
      });

      await ( newtatuatore.save(function (err, createdTodoObject)
        {
          if (err) {
            console.log(err);
          }
          // This createdTodoObject is the same one we saved, but after Mongo
          // added its additional properties like _id.
          console.log(createdTodoObject);
        })
      )

      if (nomestudio){
        await (
          Studios.findOneAndUpdate({ name: nomestudio}, { $push: { tatuatori : newtatuatore._id} }, {new: true}, function(err, doc){
            if(err){
              console.log("Something wrong when updating data!");
            }

            console.log(doc);
          })
      )
      }
    }
  }


}