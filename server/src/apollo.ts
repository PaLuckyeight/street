import * as express from "express";
import * as path from "path";

import { GraphQLSchema } from "graphql";
import { graphiqlExpress, graphqlExpress,  } from "graphql-server-express";
import { makeExecutableSchema } from "graphql-tools";
import { MuralsResolvers } from "./resolvers/muralsResover";

import Resolvers from "./resolvers";

import { readFileSync } from "fs";
import { DB } from "./db";

export class ApolloServer {

  aggragatedSchema: string[];
  executableSchema: GraphQLSchema;

  constructor(private app: express.Application, port: number) {

    this.aggragatedSchema = [readFileSync(path.join(__dirname, "..", "src/schemas/schema.graphql"), "utf8")];

    this.executableSchema = makeExecutableSchema({
      typeDefs: this.aggragatedSchema,
      resolvers: Resolvers,
      resolverValidationOptions: { requireResolversForNonScalar: false },
    });

    this.app.use("/graphql", graphqlExpress((req: express.Request) => {
      return {
        context: {
          DB,
        },
        schema: this.executableSchema,
      };
    }));

    this.app.use("/graphiql", graphiqlExpress({
      endpointURL: "/graphql",
    }));

    this.app.listen(port, () => {
      console.log(`GraphQL Server is now running on http://localhost:${port}/graphql`);
      console.log(`GraphiQL Server is now running on http://localhost:${port}/graphiql`);
    });
  }
}